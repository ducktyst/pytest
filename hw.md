
Задание:

Реализовать тесты для выбранной вами формы,  
используя данные из файла с тестовыми данными с  
различными комбинациями вводимых данных.
 
Тестовые данные хранить в json-файле. 

Ссылки:  

* https://www.lambdatest.com/blog/end-to-end-tutorial-for-pytest-fixtures-with-examples/
* https://docs.pytest.org/en/latest/fixture.html#parametrizing-fixtures

* https://realpython.com/pytest-python-testing/#:~:text=pytest%20fixtures%20are%20a%20way,that%20fixture%20as%20an%20argument.

* https://docs.python.org/3/library/json.html
* https://pythonru.com/uroki/modul-json-uroki-po-python-dlja-nachinajushhih

Дополнительно:
https://docs.pytest.org/en/reorganize-docs/yieldfixture.html
