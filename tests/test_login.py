import pytest
from selenium.webdriver.common.by import By

from selenium.webdriver.support.ui import WebDriverWait


@pytest.fixture()
def init_data():
    pass


def setup_module(module):
    print("Запустится перед выполнением пакета")


def teardown_module(module):
    print("Запустится после выполнением пакета")


def setup_function():
    pass


def teardown_function():
    pass

#
# @pytest.mark.parametrize(
#     "test_input,expected",
#     [("3+5", 8), ("2+4", 6), ("6*9", 42)])
# def test_eval(test_input, expected):
#     assert eval(test_input) == expected
#

@pytest.fixture()
def login():
    return 'LOGIN'


@pytest.fixture()
def pswd():
    return 'PSWD'


# @pytest.mark.parametrize()
def test_login(login: str, pswd: str):
    """ Используя библиотеку json заполнить параметры по аналогии с test_eval"""
    print('test_login', login * 3)
    assert 1 == 2
    # Скрипт вводящий данные

#
# def document_initialised(driver):
#     return driver.execute_script("return initialised")
#
#
# def test_explicit_wait(driver):
#     WebDriverWait(driver).until(document_initialised)
#     el = driver.find_element(By.TAG_NAME, "p")
#     assert el.text == "Hello from JavaScript!"
