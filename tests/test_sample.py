from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait


def document_initialised(driver):
    return driver.execute_script("return initialised")


def test_explicit_wait(driver):
    WebDriverWait(driver).until(document_initialised)
    el = driver.find_element(By.TAG_NAME, "p")
    print(11111)
    assert el.text == "Hello from JavaScript!"
