import json
import time

import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

from hw.config.config import input_form_data


@pytest.fixture(scope="session")
def driver(request):
    driver = webdriver.Chrome(executable_path=ChromeDriverManager().install())
    request.cls.driver = driver
    yield
    driver.close()


@pytest.fixture(scope="session")
def input_data(config) -> list:
    print('*', config)
    yield json.load(input_form_data)


def pytest_runtest_setup(item):
    """ Запустится перед выполнением каждого теста.
    """
    print("tests setting up", item)


def test_fixture_pytest(driver):
    print('start test method invocation')
    driver.get('http://google.com')
    time.sleep(3)
    print('stop test method invocation')


def setup_module(module):
    """
    https://docs.pytest.org/en/stable/xunit_setup.html?highlight=setup_module
    """
    print("Запустится перед выполнением пакета")


def teardown_module(module):
    print("Запустится после выполнением пакета")


def setup_function():
    pass


def teardown_function():
    pass
